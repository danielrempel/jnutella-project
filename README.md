## P2P file exchange program I made as a school assignment.

## Link format

Must contain:

* file hash (SHA256) in text repr (64 chars)
* filename
* size of the file
* ID of the first chunk (SHA256 in text repr, 64 ch)

Filename can't be `''` because first chunk requires some file name.

`jnutella://<filename>?hash=<hash>&size=<filesize>&fchunk=<hash>`

### File opening/addition routines

Code:

```
FilesSystem.addFile()
File f = FilesFactory.createFileFromLocal(java.io.File)
File f = FilesFactory.createFileFromLink(String)
```

### FilesFactory's actions:

#### in case of adding a local file:

* calculate hash in a separate worker thread (with progress notifications, maybe even straightly linked to UI)

* split the file into chunks and calculate hashes etc in separate thread(s)

#### link:

* parse the link and fill in fields in File

* ask user where to store the linked file

* queue downloading of the first chunk

#### both cases:

* calculate expected chunks count and prepare array of hashes